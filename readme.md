# EpfFileCleanup
EpfFileCleanup allows exporting your favorite Eclipse preferences, clean them up filtering out you local settings like paths to local resources etc, leaving only relevant preferences, which (hopefully) won't prevent them from being successfully imported into your (our other peoples) fresh, clean and untouched workspace.  

Example usage:

    java lv.mikus.eclipse.preferences.EpfFileCleanup -pref=path/to/list_of_favorite_settings.txt -df just/exported/Preferences.epf cleaned_up/result.epf