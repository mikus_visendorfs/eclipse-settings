package lv.mikus.eclipse.preferences;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.ParserProperties;

public class EpfFileCleanup {
	private static final String EPF_PREFERENCES_TO_COPY_FILE = "epf_preferences_to_copy.txt";
	private static final String INSTANCE_PREFIX = "/instance/";

	@Option(name = "-s", usage = "sort all preferences without filtering")
	private boolean sort;
	@Option(name = "-di", usage = "debug - print to console lines not starting with '" + INSTANCE_PREFIX + "'")
	private boolean debugNotInstance;
	@Option(name = "-df", usage = "debug - print to console preferences filtered out")
	private boolean debugFilteredOut;
	@Option(name = "-pref", usage = "file containing prefixes of preferences to use for filtering")
	private Path preferencesFile;
	@Argument(required = true, index = 0, metaVar = "original_epf_file", 
			usage = "Path to epf file, just exported from Eclipse via File / Export / Preferences")
	private Path originalEpfFile;
	@Argument(required = true, index = 1, metaVar = "target_epf_file", 
			usage = "Path to resulting epf file with cleaned up preferences, ready for importing into Eclipse via File / Import / Preferences")
	private Path targetEpfFile;

	public static void main(String[] args) throws Exception {
		EpfFileCleanup epfFileCleanup = new EpfFileCleanup();
		epfFileCleanup.parseArgumentsAndConvert(args);
	}

	public void parseArgumentsAndConvert(String[] args) throws IOException {
		CmdLineParser parser = new CmdLineParser(this,
				ParserProperties.defaults().withUsageWidth(120).withOptionValueDelimiter("="));
		try {
			parser.parseArgument(args);
			convert();
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java " + this.getClass().getName() + " [options...] arguments...");
			parser.printUsage(System.err);
			System.err.println();
			//System.err.print(" Example: java EpfFileCleanup" + parser.printExample(OptionHandlerFilter.ALL));
		}
	}

	public void convert() throws IOException {
		//more info on preferences http://www.vogella.com/Stringtutorials/EclipsePreferences/article.html
		List<String> originalLines = Files.readAllLines(originalEpfFile);
		Collections.sort(originalLines);
		List<String> output = new ArrayList<>(originalLines.size());
		output.addAll(getHeading());
		List<String> allowedBeginningsOfPreferences = readAllowedPreferencePrefixesFile();
		originalLines.forEach(line -> {
			if (line.startsWith(INSTANCE_PREFIX)) {
				if (sort) {
					output.add(line);
				} else if (allowedBeginningsOfPreferences.stream().anyMatch(prefix -> line.startsWith(prefix))) {
					output.add(line);
				} else if (debugFilteredOut) {
					System.out.println(line);
				}
			} else if (debugNotInstance) {
				System.out.println(line);
			}
		});
		Files.write(targetEpfFile, output);
	}

	private List<String> readAllowedPreferencePrefixesFile() throws IOException {
		try (InputStream inputStream = preferencesFile != null ? Files.newInputStream(preferencesFile)
				: getClass().getResourceAsStream(EPF_PREFERENCES_TO_COPY_FILE)) {
			return new BufferedReader(new InputStreamReader(inputStream)).lines()
					.filter(line -> !line.startsWith("#")) //filter out comments
					.collect(Collectors.toList());
		}
	}

	private List<String> getHeading() {
		return Arrays.asList(new Date().toString(), "file_export_version=3.0", "\\!/=");
	}
}
